package uz.chisto.fragments

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.navigation_drawer_fragment.*
import uz.chisto.R
import uz.chisto.adapters.NavigationDrawerAdapter
import uz.chisto.models.Category
import java.util.*

class NavigationDrawerFragment : Fragment(), AdapterView.OnItemClickListener, View.OnClickListener {

    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var drawerListener: FragmentDrawerListener? = null
    private var currentCategory: Category? = null

    private lateinit var navigationDrawerAdapter: NavigationDrawerAdapter

    fun setDrawerListener(listener: FragmentDrawerListener) {
        this.drawerListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.navigation_drawer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListView()

        tvSlogan.setOnClickListener(this)
    }

    private fun initListView() {
        if (context != null) {
            navigationDrawerAdapter = NavigationDrawerAdapter(context!!)

            drawerList.adapter = navigationDrawerAdapter
            drawerList.onItemClickListener = this
        }
    }

    fun setCategories(categories: ArrayList<Category>) {
        navigationDrawerAdapter.setCategories(categories)
    }

    fun setUp(drawerLayout: DrawerLayout, toolbar: Toolbar) {
        mDrawerToggle = object : ActionBarDrawerToggle(
            activity, drawerLayout, toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close
        ) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)

                drawerListener!!.onDrawerOpened()
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)

                toolbar.alpha = 1 - slideOffset / 2
            }
        }

        drawerLayout.setDrawerListener(mDrawerToggle)
        drawerLayout.post { mDrawerToggle!!.syncState() }
    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
        if (currentCategory == null || currentCategory!!.position != navigationDrawerAdapter.getCategories()[i].position) {
            drawerListener!!.onDrawerItemSelected(navigationDrawerAdapter.getCategories()[i])

            setCurrentCategory(navigationDrawerAdapter.getCategories()[i])
        }
    }

    override fun onClick(v: View) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://chisto.uz")))
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun setCurrentCategory(category: Category) {
        this.currentCategory = category
    }

    interface FragmentDrawerListener {

        fun onDrawerItemSelected(module: Category)

        fun onDrawerOpened()
    }
}