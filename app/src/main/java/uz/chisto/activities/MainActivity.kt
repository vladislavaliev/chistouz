package uz.chisto.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import uz.chisto.R
import uz.chisto.adapters.OffersAdapter
import uz.chisto.dialogs.ChooseOrderDialog
import uz.chisto.fragments.NavigationDrawerFragment
import uz.chisto.models.Category
import uz.chisto.models.Offer
import uz.chisto.presenters.main_activity.MainPresenter
import uz.chisto.presenters.main_activity.MainPresenterImpl
import uz.chisto.views.MainView

class MainActivity : AppCompatActivity(), MainView, NavigationDrawerFragment.FragmentDrawerListener,
    ChooseOrderDialog.ChooseOrderDialogListener, View.OnClickListener {

    private var offersAdapter: OffersAdapter? = null
    private var drawerFragment: NavigationDrawerFragment? = null

    private lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        initDrawerFragment()
        initRecyclerView()

        mainPresenter = MainPresenterImpl(this, this)
        mainPresenter.getCategories()
    }

    private fun initDrawerFragment() {
        drawerFragment = supportFragmentManager.findFragmentById(R.id.fragment_navigation_drawer) as
                NavigationDrawerFragment

        initToolbar()

        drawerFragment!!.setUp(drawer_layout, toolbar as Toolbar)
        drawerFragment!!.setDrawerListener(this)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar as Toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
    }

    private fun initRecyclerView() {
        offersAdapter = OffersAdapter(this, this)

        rv_services.layoutManager = LinearLayoutManager(this)
        rv_services.adapter = offersAdapter

//        val dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
//        dividerItemDecoration.setDrawable(getDrawable(R.drawable.rv_divider)!!)
//
//        rv_services.addItemDecoration(dividerItemDecoration)
    }

    override fun setCategories(categories: ArrayList<Category>) {
        if (drawerFragment != null) {
            drawerFragment!!.setCategories(categories)

            onDrawerItemSelected(categories[0])
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START))
            drawer_layout.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun onDrawerOpened() {}

    override fun onDrawerItemSelected(category: Category) {
        onItemClick(category)

        if (drawer_layout != null)
            drawer_layout.closeDrawer(Gravity.START)
    }

    private fun onItemClick(item: Category?) {
        if (item == null) return

        (toolbar as Toolbar).title = item.title.ru

        mainPresenter.getCategoryOffers(item)
    }

    override fun setCategoryOffers(categoryOffers: ArrayList<Offer>) {
        if (offersAdapter != null)
            offersAdapter!!.setOffers(categoryOffers)
    }

    override fun onClick(p0: View?) {
        val chooseOrderDialog = ChooseOrderDialog()
        chooseOrderDialog.setListener(this)
        chooseOrderDialog.show(supportFragmentManager, "tag")
    }

    override fun orderByTelegram() {
        val telegram = Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/chistouz_bot"))
        startActivity(telegram)
    }

    override fun orderByCall() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:+998 90 321 30 75")
        startActivity(intent)
    }
}
