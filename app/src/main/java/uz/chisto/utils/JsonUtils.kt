package uz.chisto.utils

import android.content.Context
import com.google.gson.Gson
import uz.chisto.models.Data
import java.io.IOException
import java.nio.charset.Charset

class JsonUtils {

    fun loadJSONFromAsset(context : Context): Data? {
        var json: String?

        try {
            val `is` = context.assets.open("categories.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charset.forName("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return Data(ArrayList())
        }

        return Gson().fromJson(json, Data::class.java)
    }
}