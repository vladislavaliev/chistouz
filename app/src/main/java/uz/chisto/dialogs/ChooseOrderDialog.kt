package uz.chisto.dialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.DialogFragment
import uz.chisto.R

class ChooseOrderDialog : DialogFragment(), View.OnClickListener {

    private lateinit var listener : ChooseOrderDialogListener

    fun setListener(listener: ChooseOrderDialogListener): ChooseOrderDialog {
        this.listener = listener
        return this
    }

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.choose_order_dialog, null)

        v.findViewById<CardView>(R.id.cvTelegram).setOnClickListener(this)
        v.findViewById<CardView>(R.id.cvCall).setOnClickListener(this)

        return v
    }

    override fun onClick(v: View?) {
        if(v == null) return

        if(v.id == R.id.cvTelegram)
            listener.orderByTelegram()
        else if(v.id == R.id.cvCall)
            listener.orderByCall()
    }

    interface ChooseOrderDialogListener {

        fun orderByTelegram()

        fun orderByCall()
    }
}