package uz.chisto.adapters

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.chisto.R
import uz.chisto.models.Offer
import uz.chisto.viewholders.OffersViewHolder
import java.util.*

class OffersAdapter(private val context: Context, private val listener: View.OnClickListener) :
    RecyclerView.Adapter<OffersViewHolder>() {

    private var offers = ArrayList<Offer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OffersViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.offers_list_item, parent, false)

        return OffersViewHolder(view, listener)
    }

    fun setOffers(list: ArrayList<Offer>) {
        this.offers = list

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return offers.size
    }

    override fun onBindViewHolder(holder: OffersViewHolder, position: Int) {
        val offer = offers[position]

        holder.tvName.text = offer.title.ru

        val description = offer.description.ru.replace("\n", "<br />")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            holder.tvDescription.text = Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT)
        else
            holder.tvDescription.text = Html.fromHtml(description)

        Glide.with(context).load(offer.imageUrl).centerCrop().into(holder.imageView)
    }
}