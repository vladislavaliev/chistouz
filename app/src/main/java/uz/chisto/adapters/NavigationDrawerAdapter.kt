package uz.chisto.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import uz.chisto.R
import uz.chisto.models.Category
import java.util.*

class NavigationDrawerAdapter(private var context: Context) : BaseAdapter() {

    private var categoryList = ArrayList<Category>()

    override fun getCount(): Int {
        return categoryList.size
    }

    fun setCategories(categories: ArrayList<Category>?) {
        if (categories != null) {
            this.categoryList = categories

            notifyDataSetChanged()
        }
    }

    fun getCategories() : ArrayList<Category> {
        return categoryList
    }

    override fun getItem(i: Int): Any {
        return categoryList[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        var view = view
        val holder: ViewHolder

        if (view == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.navigation_drawer_list_item, viewGroup, false)

            holder = ViewHolder()

            holder.icon = view!!.findViewById(R.id.icon)
            holder.title = view.findViewById(R.id.title)

            view.tag = holder
        } else
            holder = view.tag as ViewHolder

        if(categoryList[i].position == 0)
            holder.icon!!.visibility = View.GONE

        holder.title!!.text = categoryList[i].title.ru

        return view
    }

    internal class ViewHolder {

        var icon : ImageView? = null
        var title: TextView? = null
    }
}