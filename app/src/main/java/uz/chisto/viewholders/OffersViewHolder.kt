package uz.chisto.viewholders

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uz.chisto.R

class OffersViewHolder(itemView: View, listener: View.OnClickListener) : RecyclerView.ViewHolder(itemView) {

    var tvName: TextView = itemView.findViewById(R.id.tvName)
    var tvDescription: TextView = itemView.findViewById(R.id.tvDescription)
    var imageView: ImageView = itemView.findViewById(R.id.imageView)
    var btnOrder: Button = itemView.findViewById(R.id.btnOrder)

    init {
        btnOrder.setOnClickListener(listener)
    }
}