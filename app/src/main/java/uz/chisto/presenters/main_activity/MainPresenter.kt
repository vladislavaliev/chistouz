package uz.chisto.presenters.main_activity

import uz.chisto.models.Category

interface MainPresenter {

    fun getCategories()

    fun getCategoryOffers(category: Category)
}