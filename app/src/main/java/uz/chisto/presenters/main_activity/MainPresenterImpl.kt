package uz.chisto.presenters.main_activity

import android.content.Context
import uz.chisto.models.Category
import uz.chisto.models.Offer
import uz.chisto.models.Text
import uz.chisto.utils.JsonUtils
import uz.chisto.views.MainView

class MainPresenterImpl(private val context: Context, private val view: MainView) : MainPresenter {

    override fun getCategories() {
        val data = JsonUtils().loadJSONFromAsset(context)

        if (data?.categories != null) {
            var categories = data.categories

            // Добавляем категорию "Все"
            categories.add(0, generateGeneralCategory(categories))

            view.setCategories(categories)
        }
    }

    private fun generateGeneralCategory(categories : ArrayList<Category>) : Category {
        var generalCategoryOffers = ArrayList<Offer>()

        categories.forEach {
            generalCategoryOffers.addAll(it.offers!!)
        }

        return Category(0, Text("ВСЁ ЧИСТО", null), generalCategoryOffers)
    }

    override fun getCategoryOffers(category: Category) {
        if (category.offers != null)
            view.setCategoryOffers(category.offers)
    }
}