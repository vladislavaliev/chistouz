package uz.chisto.views

import uz.chisto.models.Category
import uz.chisto.models.Offer

interface MainView {

    fun setCategories(categories : ArrayList<Category>)

    fun setCategoryOffers(categoryOffers : ArrayList<Offer>)
}