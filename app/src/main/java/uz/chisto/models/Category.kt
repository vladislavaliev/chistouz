package uz.chisto.models

data class Category(
    val position: Int,
    val title: Text,
    val offers: ArrayList<Offer>?
)