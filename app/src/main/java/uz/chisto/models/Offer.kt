package uz.chisto.models

data class Offer(
    val imageUrl: String,
    val pricePerUnit: Long,
    val minOrderQty: Int,
    val minOrderMoney: Long,
    val measurementUnit: Text,
    val title: Text,
    val description: Text
)